
# <center> LeaRnV: Teaching Embedded System Design using RISC-V</center>
### <center> ![CI/CD Status](https://gricad-gitlab.univ-grenoble-alpes.fr/tima-amfors/learnv/badges/master/pipeline.svg) </center>

This repository hosts the official LeaRnV website.

An introduction page is hosted in the following link  https://tima-amfors.gricad-pages.univ-grenoble-alpes.fr/learnv/ 

## Documents

We provide the following resources :

- [Presentation](Downloads/demo_presentation.pdf): a PDF file containing the presentation of the demo.

- [Environment Setup](Downloads/environment_setup.pdf): a PDF file containing a manual to setup the working environment.
						  In particular, you will find in this file how and where toinstall the different archives listed below.

- [Practical Labs](Downloads/lab_subject.pdf) : a PDF file containing all the subjects of the practical labs, as well as all the documentation required to complete these labs.

- [lowRISC Project Subject](Downloads/lowrisc_project_subject.pdf): a PDF file containing a full description of the project.

- [lowRISC Project - Technical Documentation](Downloads/lowRISC_project.pdf): a PDF file containing the technical documentation associated with the project, necessary to understand the hardware part of it.

## Labs and Project Archives

- [TPSoC_3A_Sys_2020.zip](https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing): archive containing the Virtual Disk.

- [TPSoC_Resources.tar.xz](https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing): archive containing the SW/HW toolchain that will be used during the labs, such as Rocket-Chip, GCC, Spike, and others.

- [TP_Code_Source.zip](https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing) : archive containing the templates of the labs.

- [lowrisc-chip-DATE2020-Student](https://drive.google.com/drive/folders/1hrl2jdR2Vth-LHSbGfdPNsoaX_dAuQHE?usp=sharing) : archive containing the project template.


### Contact

To request Labs solutions or for any adminstrative question, please contact [Dr. Mounir Benabdenbi](mailto:Mounir.Benabdenbi@univ-grenoble-alpes.fr).

For technical questions, please contact [Noureddine Ait Said](noureddine.ait-said@univ-grenoble-alpes.fr) or open an issue in this repository.

### Reference this work

```latex
@article{ learnv,
	author = {{Ait Said, N.} and {Benabdenbi, M.} and {Villanova Magalh\~aes, G.}},
	title = {Prototypage Mat\'eriel-Logiciel de Syst\`emes Int\'egr\'es avec l'architecture RISC-V},
	DOI= "10.1051/j3ea/20191016",
	url= "https://doi.org/10.1051/j3ea/20191016",
	journal = {J3eA},
	year = 2019,
	volume = 18,
	pages = "1016",
}
```

### Licensing

This website and all the provided source code and resources are released under the Apache 2.0 license. Please refer to the license file for further information.
